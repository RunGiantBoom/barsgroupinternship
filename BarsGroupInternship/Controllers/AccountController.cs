﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BarsGroupInternship.Models;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Web.Security;

namespace BarsGroupInternship.Controllers
{
    public class AccountController : Controller
    {
        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UserModel user;
                using (ISession session = NHibertnateSession.OpenSession())
                {
                    user = session.CreateCriteria<UserModel>()
                           .Add(Expression.Like("Login", model.UserName))
                           .UniqueResult<UserModel>();
                }

                //Сравниваем голые пароли, пока без хеша.
                if (user != null)
                {
                    if (user.Password == model.Password)
                    {
                        FormsAuthentication.SetAuthCookie(user.Login, true);
                        
                        //Лучше бы так не делать, ибо подменить на клиенте можно на раз-два
                        Session["id"] = user.Id;

                        //Если клиент залогинился как читатель, то редиректим нa ReaderController, если как библиотекарь, то на Librarian
                        //Пока через строки вместо enum

                        if (user.Role == "librarian")
                        {
                            return Redirect(Url.Action("Index", "Librarian"));
                        }
                        else if (user.Role == "reader")
                        {
                            return Redirect(Url.Action("Index", "Reader"));
                        }
                        else
                        {
                            ModelState.AddModelError("", "Incorrect Role. Please, contact with administrator");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Incorrect password");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username not exists");
                }
                return View();
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(UserModel user)
        {
            if (ModelState.IsValid)
            {
                using (ISession session = NHibertnateSession.OpenSession())
                {
                    //First тут хуже смотрится чем UnigueResult, но и лист использовать и проверять его размер на случай дубликатов тоже не хорошо
                    //Будем надеяться что не будет ошибок из-за которых появятся дубликаты логинов в базе
                    //var dbuser = session.QueryOver<UserModel>().Where(p => p.Login == user.Login).;

                    UserModel dbuser = session.CreateCriteria<UserModel>()
                        .Add(Expression.Like("Login", user.Login))
                        .UniqueResult<UserModel>();

                    if (dbuser != null)
                    {
                        ModelState.AddModelError("", "User exists");
                        return View();
                    }

                    session.Transaction.Begin();
                    session.Save(user);
                    session.Transaction.Commit();
                }
                return RedirectToAction("Login");
            }
            else
            {
                return View();
            }
        }
    }
}