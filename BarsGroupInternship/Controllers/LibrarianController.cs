﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BarsGroupInternship.Models;
using NHibernate;
using NHibernate.Linq;
using System.Diagnostics;

namespace BarsGroupInternship.Controllers
{
    public class LibrarianController : Controller
    {
        // GET: Librarian
        //[Authorize(Users = "librarian")]
        //[Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        ///////////////////////////////////Обработка книг

        [HttpGet]
        public ActionResult GetBooksList()
        {
            using (ISession session = NHibertnateSession.OpenSession())
            {
                var books = session.Query<Book>().ToList();
                return View(books);
            }
        }

        // GET: Default/Create
        [HttpGet]
        public ActionResult BookCreate()
        {
            return View();
        }

        // POST: Default/Create
        [HttpPost]
        public ActionResult BookCreate(Book book)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (ISession session = NHibertnateSession.OpenSession())
                    {
                        session.Transaction.Begin();
                        session.Save(book);
                        session.Transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    Debug.Print(e.Message);
                }
                return View();
            }
            else
            {
                return View();
            }
        }

        // GET: Default/Edit/5
        [HttpGet]
        public ActionResult BookEdit(int id)
        {
            Book book;
            using (ISession session = NHibertnateSession.OpenSession())
            {
                book = session.Get<Book>(id);
            }
            return View(book);
        }

        // POST: Default/Edit/5
        [HttpPost]
        public ActionResult BookEdit(int id, Book book)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (ISession session = NHibertnateSession.OpenSession())
                    {
                        session.Transaction.Begin();
                        session.Update(book);
                        session.Transaction.Commit();
                    }
                    return RedirectToAction("GetBooksList");
                }
                catch (Exception e)
                {
                    Debug.Print(e.Message);
                    return View(book);
                }
            }
            else
            {
                return View(book);
            }
        }

        // GET: Default/Delete/5
        public ActionResult BookDelete(int id)
        {
            using (ISession session = NHibertnateSession.OpenSession())
            {
                //Нет удаления по id? Не верю, надо почитать ибо делать свой селект здесь не по гайдлайну.
                Book book = session.Get<Book>(id);

                session.Transaction.Begin();
                session.Delete(book);
                session.Transaction.Commit();
            }
            return RedirectToAction("GetBooksList");
        }

        ///////////////////////////////////Обработка читателей

        [HttpGet]
        public ActionResult GetReadersList()
        {
            using (ISession session = NHibertnateSession.OpenSession())
            {
                var Users = session.Query<UserModel>().ToList();
                return View(Users);
            }
        }
        
        
        // GET: Default/Create
        [HttpGet]
        public ActionResult ReaderCreate()
        {
            return View();
        }

        // POST: Default/Create
        [HttpPost]
        public ActionResult ReaderCreate(UserModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (ISession session = NHibertnateSession.OpenSession())
                    {
                        session.Transaction.Begin();
                        session.Save(user);
                        session.Transaction.Commit();
                    }
                }
                catch (Exception e)
                {
                    Debug.Print(e.Message);
                }
                return View();
            }
            else
            {
                return View();
            }
        }


        // GET: Default/Edit/5
        public ActionResult ReaderEdit(int id)
        {
            UserModel User;
            using (ISession session = NHibertnateSession.OpenSession())
            {
                User = session.Get<UserModel>(id);
            }
            return View(User);
        }

        // POST: Default/Edit/5
        [HttpPost]
        public ActionResult ReaderEdit(int id, UserModel user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (ISession session = NHibertnateSession.OpenSession())
                    {
                        session.Transaction.Begin();
                        session.Update(user);
                        session.Transaction.Commit();
                    }
                    return RedirectToAction("GetReadersList");
                }
                catch (Exception e)
                {
                    Debug.Print(e.Message);
                    return View(user);
                }
            }
            else
            {
                return View(user);
            }
        }

        // GET: Default/Delete/5
        public ActionResult ReaderDelete(int id)
        {
            using (ISession session = NHibertnateSession.OpenSession())
            {
                UserModel user = session.Get<UserModel>(id);

                session.Transaction.Begin();
                session.Delete(user);
                session.Transaction.Commit();
            }
            return RedirectToAction("GetReadersList");
        }

        //Остались методы для обработки очереди и заявок

    }
}