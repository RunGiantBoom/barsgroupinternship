﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BarsGroupInternship.Models;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;

namespace BarsGroupInternship.Controllers
{
    public class ReaderController : Controller
    {
        // GET: Reader
        //[Authorize(Users = "reader")]
        public ActionResult Index()
        {
            //Временные переменные, для любопытства, что там
            bool aslibrarian = HttpContext.User.IsInRole("librarian");
            bool asreader = HttpContext.User.IsInRole("reader");
            bool IsAuth = HttpContext.User.Identity.IsAuthenticated;
            string login = HttpContext.User.Identity.Name;

            var cook = HttpContext.Request.Cookies["cookies"];
            int id = (int)Session["id"];

            //Возвращать здесь страницу со списком имеющихся книг и 
            //две ссылки внизу, на поиск книг и на бронирование
            IList<Book> books;

            using (ISession session = NHibertnateSession.OpenSession())
            {
                books = session.CreateCriteria<Book>()
                       .Add(Expression.Like("ReaderId", id))
                       .List<Book>();
                return View(books); //Возвращаем те книги, которые заняты текущим читателем.
            }
        }
        [HttpGet]
        public ActionResult BookSearchOptions()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BookSearchOptions(Book bookExample)
        {
            return RedirectToAction("Login", bookExample);
            //return BookSearch(bookExample);
        }

        [HttpGet]
        public ActionResult BookSearch(Book bookExample)
        {
            if (bookExample == null)
            {
                bookExample = new Book();
            }
            IList<Book> books;

            using (ISession session = NHibertnateSession.OpenSession())
            {
                //session.Query<Book>().Where(x => x.Author == bookExample.Author).First();
                //Expression.Like("Name", "Fritz%")
                //books = session.CreateCriteria<Book>()
                //       .Add( Example.Create(bookExample).ExcludeProperty("Id"))
                //       .List<Book>();
                var criteria = session.CreateCriteria<Book>();
                criteria = criteria.Add(Expression.Like("ReaderId", null));
                if (bookExample.Name != null) criteria.Add(Expression.Like("Name", bookExample.Name));
                if (bookExample.Author != null) criteria.Add(Expression.Like("Author", bookExample.Author));
                books = criteria.List<Book>();

                //books = session.CreateCriteria<Book>()
                //   .Add(Expression.Like("Name", bookExample.Name))
                //   .Add(Expression.Like("Author", bookExample.Author))
                //   .List<Book>();
                return View(books); //Возвращаем те книги, которые заняты текущим читателем.
            }
        }

        public ActionResult BookReserve(int id)
        {
            //Здесь нужно создать в базе объект очереди, 
            return RedirectToAction("Index");
        }
    }
}