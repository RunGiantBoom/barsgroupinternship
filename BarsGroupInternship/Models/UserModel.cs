﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BarsGroupInternship.Models
{
    public class UserModel
    {
        public virtual int Id { get; set; }

        [Required]
        public virtual string Role { get; set; }

        [Required]
        public virtual string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public virtual string Password { get; set; }
        
        [Required(ErrorMessage = "Please enter name")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Please enter surname")]
        public virtual string Surname { get; set; }
    }
}