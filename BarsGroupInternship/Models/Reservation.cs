﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BarsGroupInternship.Models
{
    public class Reservation
    {
        public virtual int Id { get; set; }
        public virtual int BookId { get; set; }
        public virtual int ReaderId { get; set; }
    }
}