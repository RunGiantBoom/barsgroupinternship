﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BarsGroupInternship.Models
{
    public class Book
    {
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Please enter name")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Please enter Author")]
        public virtual string Author { get; set; }

        public virtual int ReaderId { get; set; }

    }
}