﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using NHibernate.Cfg;

namespace BarsGroupInternship.Models
{
    public class NHibertnateSession
    {
        public static ISession OpenSession()
        {
            var configuration = new Configuration();
            var configurationPath = HttpContext.Current.Server.MapPath(@"~\Web.config");
            configuration.Configure(configurationPath);
            var booksConfigurationFile = HttpContext.Current.Server.MapPath(@"~\Models\Book.hbm.xml");
            configuration.AddFile(booksConfigurationFile);
            configuration.AddFile(HttpContext.Current.Server.MapPath(@"~\Models\UserModel.hbm.xml"));
            ISessionFactory sessionFactory = configuration.BuildSessionFactory();
            return sessionFactory.OpenSession();
        }
    }
}